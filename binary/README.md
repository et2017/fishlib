# Prerequisite
需要PFC 5.00.21或22版本，其它较早版本没有经过测试。

# 使用说明
将这个dll文件拷贝到C:\Program Files\Itasca\PFC500\exe64（替换为你电脑上的安装路径）下
在你的脚本中添加如下命令。

```
load contactmodelmechanical linearpbondu
```

使用上和内建的linearpbond模型一样，**注意模型名称多了一个字母`u`**，代表user-defined model。
