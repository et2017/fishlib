; fname: Footing.p2dat
;
;  Create an assembly and install a shallow foundation
;==============================================================================

new

;Set the domain extent and condition
domain extent (-1,25) (-6,20)
domain condition destroy

;generate a box and a cloud of 2000 equally sized balls
wall generate box (0,24) (0,17)
set random 10001
ball generate box (0,24) (0,10) number 2000 radius 0.1250

; Modify the default slots of the Contact Model Assignement Table
; Here we choose the linear contact model (with kn=1e6) for all contact types but
; ball-ball contacts, to which we assign the linearpbond model.
; Note that only the linear part of the linearpbond model is activated as long as the 
; contact method bond is not called, which is done later
cmat default type ball-ball model linearpbond   ...
        property fric 0.577 kn 1e8 ks 1e8       ...
        pb_kn 1e8 pb_ks 1e8 pb_ten 1e6          ...
        pb_coh 1e6 pb_rmul 0.8 dp_nratio 0.2

cmat default type ball-facet model linear       ...
    property kn 1e8 ks 1e8 fric 0.09            ...
    dp_nratio 0.2

;define the density of the balls and increase their radius
ball attribute density 2000 radius multiply 1.4

;define some history to plot the velocity of a ball of the assembly and 
;of the unbalanced force. Histories are stored each 5 cycles.
set hist_rep 5
history ball yvelocity 12 5
history mechanical solve unbalanced

; To reach a stable configuration faster, density scaling is activated.
; Gravity is introduced
set timestep scale
set gravity 0 -9.81
cycle 1000 calm 50
solve aratio 1e-5

; creation of a box-shaped foundation
; the name 'footing' is associated to the respective group of walls
wall delete range set id 3
wall generate group footing box (0,5) (11,20)

; parallel bonds are finally installed. LinearPBond model is active
; for ball-ball contacts. The simulation is saved at this stage
; so that further restoring is possible
contact method bond gap 0.0

save Assembly

; The foundation is supposed to settle with a constant velocity
; The contact force at the base of the foundation is stored during the simulation
wall attribute yvelocity -0.25 range group footing
history wall ycontactforce id 7

; The timestep is set to its automatic value for dynamic simulations and
; an additional 5 seconds are simulated. The model is saved at the end of the simulation.
set timestep auto
solve time 10.0

; Solve to an equilibrium state again
wall attribute yvelocity 0.0
solve aratio 1e-4

save Footing

;==============================================================================
; eof: Footing.p2dat

