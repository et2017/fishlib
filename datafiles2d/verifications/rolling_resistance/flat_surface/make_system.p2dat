;==============================================================================
; fname: make_system.p2dat
;==============================================================================
new
title 'Rolling Resistance Linear Contact Model: Single Ball on a Flat Surface'

; define domain extent and boundary conditions
domain extent -1.0 1.0 -1e-1 2e-1 condition destroy

; set the defaults slots of the CMAT to use the rrlinear contact model
cmat default model rrlinear method deformability emod 1e7 kratio 1.0 ...
                            property dp_nratio 0.2 dp_mode 3

; create the system and settle under gravity
wall generate plane
ball create radius 5e-3 y 5e-3
ball attribute density 2500.0
set gravity 9.81
solve

; set up history monitoring
set mechanical age 0.0
set orientation on
set energy on
history nstep 1
history id  1 mechanical age
history id  2 mechanical energy ekinetic
history id  3 mechanical energy eslip
history id  4 mechanical energy errslip
history id  5 mechanical energy edashpot
history id  6 mechanical energy estrain
history id  7 mechanical energy errstrain
history id 11 ball xposition id 1
history id 12 ball spin id 1
history id 13 ball xvelocity id 1
ball attribute xvelocity 1.0
save system-ini
return
;==============================================================================
; eof: make_system.p2dat