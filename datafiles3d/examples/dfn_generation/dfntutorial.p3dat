;-----------
;  CODE 1
;-----------

new

; define the domain
domain extent -50 50 -50 50 -50 50

; fix the random seed
set random 101

;-----------
;  CODE 2
;----------- 

; borehole creations 
geom set vertical_borehole
geom edge (0,0,-45) (0,0,40)

geom set 45_degree_borehole
def make_45_borehole
   local x1 = -50
   local x2 = 50
   ; rotation of 45 degree
   local x1rot = x1*math.cos(math.pi/4)
   local z1rot = x1*math.sin(math.pi/4)
   local x2rot = x2*math.cos(math.pi/4)
   local z2rot = x2*math.sin(math.pi/4)
   command 
       geom edge (@x1rot,0,@z1rot) (@x2rot,0,@z2rot)
   endcommand
end
@make_45_borehole

; outcrop creation 
geom set horizontal_outcrop
geom poly pos (-40,-40,40) (-40,40,40) (40,40,40) (40,-40,40)

geom set vertical_outcrop
geom poly pos (-41,-41,50) (-41,41,50) (-41,41,40) (-41,-41,40)

save geometries


;------------
;  CODE 3
;------------

; define the template of set 1: vertical fractures
dfn template create name sub_vertical orientation fisher 90,120,200 size powerlaw 4 slimit 10 500
dfn generate name sub_vertical genbox -100 100 -100 100 -100 100 id 1 template name sub_vertical density 0.33

;-----------
;  CODE 4
;-----------

; define the template of set 2: horizontal fractures
dfn template create name sub_horizontal orientation fisher 20 30 500 size powerlaw 4 slimit 10 500

define variables
  global nb_hori = 0
  global nb_hori_aimed = 39
end
@variables

define hori_study(frac)
  local outc = geom.set.find('vertical_outcrop')
  local is_inter = dfn.fracture.gintersect(frac,outc)
  if is_inter>0
    nb_hori = nb_hori+1
  endif
end

define hori_stop
    hori_stop = 0
    if nb_hori >= nb_hori_aimed
        hori_stop=1
    endif
end

dfn generate name sub_horizontal genbox -100 100 -100 100 -100 100 id 2 template name sub_horizontal modif @hori_study stopfish @hori_stop

;-----------
;  CODE 5
;-----------

; define the template of set 3: background jointing
dfn template create name background orientation bootstrapped orientation_distribution.inp size powerlaw 3.2 slimit 2 10
dfn generate name background genbox -55 55 -55 55 -55 55 id 3 template name background p10geom 0.5 vertical_borehole


save dfngeneration

;-----------
;  CODE 6
;-----------

; print densities

dfn information density
dfn information p10 p10begin (-50,0,0) p10end (50,0,0)
define geometry_density
    local poutcrop = geom.set.find('vertical_outcrop')
    local pborehole = geom.set.find('vertical_borehole')
    local oo = io.out('p21 vertical = '+string(dfn.geomp21(dfn.fracture.list,poutcrop)))
    oo = io.out('p10 vertical = '+string(dfn.geomp10(dfn.fracture.list,pborehole)))
end
@geometry_density

;-----------
;  CODE 7
;-----------

; traces and intercepts
dfn intersection id 1 name inter_hori_out geomname horizontal_outcrop groupslot 1
dfn intersection id 2 name inter_vert_out geomname vertical_outcrop groupslot 2
dfn intersection id 3 name inter_vert_bore geomname vertical_borehole groupslot 3

define assign_extras
    local set1 = dfn.setinter.find(1)
    local intlist = dfn.setinter.interlist(set1)
    loop foreach e1 intlist
        local f1 = dfn.inter.end1(e1)
        if type.pointer.id(f1) = dfn.fracture.typeid then
            dfn.fracture.extra(f1,1) = 1
        endif
    endloop    
    local set2 = dfn.setinter.find(2)
    intlist = dfn.setinter.interlist(set2)
    loop foreach e1 intlist
        f1 = dfn.inter.end1(e1)
        if type.pointer.id(f1) = dfn.fracture.typeid then
            dfn.fracture.extra(f1,2) = 2
        endif
    endloop
    local set3 = dfn.setinter.find(3)
    intlist = dfn.setinter.interlist(set3)
    loop foreach e1 intlist
        f1 = dfn.inter.end1(e1)
        if type.pointer.id(f1) = dfn.fracture.typeid then
            dfn.fracture.extra(f1,3) = 3
        endif
    endloop
end 

@assign_extras

save traces

;-----------
;  CODE 8
;-----------

; connectivity

dfn intersection id 5 name all
dfn cluster interset id 5 groupslot 1

save connectivity

;-----------
;  CODE 9
;-----------

; keep only infinite cluster
dfn intersection delete
dfn delete range group "all1" not
dfn intersection id 5 name all

;-----------
;  CODE 10
;-----------

; filter by connected distance

geom set injection_section
geom poly pos (-1,-1,-1) (-1,1,-1) (1,1,-1) (1,-1,-1)
geom poly pos (-1,-1,-1) (-1,1,-1) (-1,1,1) (-1,-1,1)
geom poly pos (-1,-1,1) (-1,1,1) (1,1,1) (1,-1,1)
geom poly pos (1,1,1) (1,-1,1) (1,-1,-1) (1,1,-1)
geom poly pos (1,1,1) (1,1,-1) (-1,1,-1) (-1,1,1)
geom poly pos (1,-1,1) (1,-1,-1) (-1,-1,-1) (-1,-1,1) 
dfn connectivity distance extra 6 geometry injection_section interset id 5
dfn intersection delete
dfn delete range extra 6 0 50 not

save simplification1

;-----------
;  CODE 10
;-----------

; fracture combination

dfn combine distance 1 angle 20 merge range extra 6 10 20 
dfn combine distance 5 angle 45 merge range extra 6 20 30 
dfn combine distance 10 angle 80 merge range extra 6 30 50 

save simplification2
