; fname: BlenderClusters.p3dat

new
wall resolution full

call ClusterMakeCluster
call ClusterSizeDistribution
call ../blender_geometry/ImportBlenderGeometry

geometry import ellipsoid.stl

domain extent [lbx-0.05*dx] [ubx+0.05*dx] [lby-0.05*dy] [uby+0.05*dy] [lbz-0.05*dz] [ubz+dz] ...
        condition destroy 

wall import geom rotor1 clean id 1
wall import geom rotor2 clean id 2
wall import geom rotor3 clean id 3
wall import geom rotor4 clean id 4
wall import geom rotor5 clean id 5
wall import geom rotor6 clean id 6
wall import geom rotor7 clean id 7
wall import geom rotor8 clean id 8

wall import geom blade1 clean id 9
wall import geom blade2 clean id 10
wall import geom blade3 clean id 11
wall import geom blade4 clean id 12
wall import geom blade5 clean id 13
wall import geom blade6 clean id 14
wall import geom blade7 clean id 15
wall import geom blade8 clean id 16
wall group facetassembly facet
wall group wallassembly

wall import geom bin clean id 17
wall activeside top range z [ubz-0.05*ubz] [ubz+0.05*ubz]
wall activeside top range name facetassembly

def setup(rpm)
  rp_ = vector(0,0,0)
  spin_ = vector(0,-1,0)*2*math.pi*rpm/60.0
  knB_ = 1e4 
  ksB_ = 1e4 
  knW_ = 1e8
  ksW_ = 1e8
  fric_ = 0.18
  dens_ = 2000
end
@setup(60)

clump template create name pill geometry ellipsoid surfcalc bubblepack ratio 0.5 distance 150
    
cmat default type pebble-pebble model linear property kn 10e4 inh off ...  
                                                      ks 10e4 inh off ...
                                                      fric 0.18
cmat default type pebble-facet model linear property kn 10e5 inh off ...
                                                     ks 10e5 inh off ...
                                                     fric 0.09

clump distribute porosity 0.45                  ...
                        numbin    1             ...
                        diameter                ...
                        resolution 1.0          ...
                        bin 1 size 0.05 0.075   ...
                               volumefrac    1  ...
                               template pill    ...
                               az 90 90         ...
                               el 0 0           ...
                               tilt 0 0         ...  
                        box [lbx] [ubx] [lby] [uby] [lbz] [lbz+0.3*dz]

def inBlender(pos,c)
  inBlender = 1
  if type.pointer.id(c) = clump.typeid
    loop foreach local pb clump.pebblelist(c)
      local tmp = 0
      wp = wall.find(17)
      tmp = wall.inside(wp,clump.pebble.pos(pb))
      if tmp = 0
        inBlender = 0
        exit
      endif
    endloop
  endif
end
clump delete range fish @inBlender not

def inBody(pos,c)
  inBody = 0
  if type.pointer.id(c) = clump.typeid
    loop foreach local pb clump.pebblelist(c)
      local tmp = 0
      loop foreach local wp wall.list
        if wall.id(wp) # 17
          tmp = wall.inside(wp,clump.pebble.pos(pb))
          if tmp = 1
            inBody = 1
            exit
          endif
        endif
      endloop
    endloop
  endif
end
clump delete range fish @inBody

set gravity 0,0,-9.81
clump attr density @dens_ damp 0.7

cycle 2000 calm 500

@ini_clusters
@replace
@order_clusters
@list_clusters

define inclump(extra, ct)
  inclump = 0
  if type.pointer.id(ct) = contact.typeid("ball-ball")
    if ball.extra(contact.end1(ct),1) == ball.extra(contact.end2(ct),1) then
      inclump = 1
      exit
    endif
  endif
end

cmat 	default type ball-ball model linear ... 
        property kn 10e4 inh off ks 10e4 inh off fric 0.18 inh off
cmat 	default type ball-facet   model Linear ... 
        property kn 10e5 inh off ks 10e5 inh off fric 0.09 inh off
cmat	add 1 model linearPBond ... 
        property fric 0.0 kn 0.0 inh off ks 0.0 inh off dp_nratio 0.2 ...
        range fish @inclump 

clean
cmat apply

@applybonds

def restorefriction(arr)
    ct=arr(1)
    ;local oa = out(string.build("extra1 %1 - extra2 %2",ball.extra(c_end1(ct),1),ball.extra(c_end2(ct),1)))
    if ball.extra(contact.end1(ct),1) == ball.extra(contact.end2(ct),1)
      io.out(string.build("Contact broken! %1",contact.prop(ct,'pb_state')))
      contact.model(ct) = 'Linear'
      contact.prop(ct,'kn') = 10e4
      contact.prop(ct,'ks') = 10e4
      contact.prop(ct,'lin_mode') = 1
      
      idc = idc + 1     
      blist = array.create(1)
      blist(1) = contact.end1(ct)
      size = array.size(blist,1)
      ind = 0
      stop = 0
      loop while stop = 0
      
          newbonds = 0
          newvisited = 0
          loop local i (1,size+ind)
            ;local oppz = out(string.build("blist(i) %1",blist(i)))
              if ball.extra(blist(i),2) = 0 ;isNOTvisited
                  newvisited = newvisited + 1
                  loop foreach local con ball.contactmap(blist(i))
                    if contact.model(con) # "Linear"
                      if contact.prop(con,'pb_state')=3
                          if ball.extra(contact.end1(con),2) = 0 ;not visited
                              if ball.extra(contact.end2(con),2) = 0 ;not visited
                                  newbonds = newbonds + 1
                                  ;local ff = out(string.build("ball ids = %1 %2",ball.id(c_end1(con)),ball.id(c_end2(con))))
                              endif
                          endif
                      endif
                    endif
                 endloop
                  ball.extra(blist(i),2) = 1 ;isvisited
                  ball.extra(blist(i),1) = idc
              endif
          endloop
          
        ;local ops = out(string.build("newbonds %1",newbonds))
        if newbonds#0
          oldblist = blist
          blist = array.create(size+ind+newbonds)
          loop local k (1,size+ind)
            blist(k) = oldblist(k)
          endloop
          
          loop local ii (size+ind+1-newvisited,size+ind)
            clist = ball.contactmap(blist(ii))
            loop foreach local jj clist
             if contact.model(jj) # "linear"
              if contact.prop(jj,'pb_state')=3
                if ball.extra(contact.end1(jj),2) = 0 ;not visited
                  ind = ind + 1
                  blist(size+ind) = contact.end1(jj)
                else if ball.extra(contact.end2(jj),2) = 0 ;not visited
                  ind = ind + 1
                  blist(size+ind) = contact.end2(jj)
                endif
              endif
             endif
            endloop
          endloop
        else
          stop = 1
        endif
      endloop
    endif
    
    loop foreach local bp ball.list
      ball.extra(bp,2) = 0 ;not visited
    endloop
end

ball attr density 2000 damp 0.7

set fish callback bond_break @restorefriction

set timestep auto
cycle 2000 calm 500
ball attr damp 0.0

wall attr centrotation @rp_ range group wallassembly
wall attr spin @spin_ range group wallassembly

def initializeTime
  mp_age_c = mech.age
  global tinc = 0.01
  global tnext = tinc
end
@initializeTime

def buildPSD
  whilestepping
  global temps = mech.age-mp_age_c
  if temps > tnext then
    sizedistribution(20)
    tnext = tnext + tinc
  endif
end

@sizedistribution(20)

save cluster_model

solve time 0.1

save end_blendercluster

;==============================================================================
; eof: BlenderClusters.p3dat