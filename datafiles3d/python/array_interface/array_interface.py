 
import itasca
from itasca import ballarray as ba
import numpy as np



dim = 2.5e-2
rad = dim / 20.0
itasca.command("""
new
domain extent -5e-2 6e-2 -6e-2 5e-2 -5e-2 5e-2
cmat default model linear property kn 1e1 dp_nratio 0.2
ball generate cubic box -{xdim} {xdim} -{ydim} {ydim} -{xdim} {xdim} rad {rad}
ball attr dens 2600
""".format(xdim=dim-rad, ydim=2*dim-rad, rad=rad))


radii = ba.radius()


radii


assert type(radii) is np.ndarray
print radii.shape
print (itasca.ball.count(),)


radii *= 0.75
ba.set_radius(radii)
radii = ba.radius()


radii


positions = ba.pos()


positions


force = -5e-3 * (positions.T/np.linalg.norm(positions,axis=1)).T


force


ba.set_force_app(force)



x, y, z = positions.T



y


y>0


radii[y>0] /= 0.75
ba.set_radius(radii)

