; fname: Hopper.p3dat
;==============================================================================
new
title 'Simple hopper discharge model'

define geometry
  W = 20.0
  W0 = 6.0
  Theta = 30
  H = 30.0
  A = (W-W0)*math.tan(Theta*math.pi/180)
end
@geometry

domain extent ([-W*1.5],[W*1.5]) ([-W*1.5],[W*1.5]) ([-H*2],[H*2])
domain condition destroy

wall generate id 1 cylinder base (0.0,0.0,@A) height [H-A] radius [W*0.5] cap false false onewall
wall generate id 2 cone radius [W0*0.5] [W*0.5] height @A cap true false 

cmat default type ball-ball model linear ...
     property kn 5e7 ks 5e7 fric 0.577 dp_nratio 0.2

cmat default type ball-facet model linear ...
    property kn 1e8 ks 1e8 fric 0.1 dp_nratio 0.2

set random 10001
ball distribute porosity 0.5 box ([-W*0.35],[W*0.35]) ([-W*0.35],[W*0.35]) (@A,[1.8*H]) radius 0.8 1.0
ball attribute density 1000.0 damp 0.7

set gravity 0 0 -9.81
cycle 1000 calm 50
set timestep scale
solve aratio 1e-3

ball delete range cylinder end1 (0.0,0.0,0.0) end2 (0.0,0.0,@H) radius 0.0 [W*0.5] not 
ball group LevelOne range z 0.0 [H/6]
ball group LevelTwo range z [H/6][2*H/6]
ball group LevelThree range z [2*H/6][3*H/6]
ball group LevelFour range z [3*H/6][4*H/6]
ball group LevelFive range z [4*H/6][5*H/6]
ball group LevelSix range z [5*H/6][H*2]

wall delete range z 0.0 set id 2
wall generate id 200 box ([-W*1.25],[W*1.25]) ([-W*0.25],[W*0.25]) ([-H*1.5],0.0)
wall delete range set id 201

save initial

define makeMovie(dur,inc,name)
  i = 0
  curv = inc
  dur = dur
  namefile = name
  loop while (curv <= dur)
    i = i + 1
    dist = 150.0/(i*0.01)
    if dist > 150.0
      dist = 150.0
    endif
    local fname = string.build('%1_%2.png',name,i)
    command
      ball result load time [curv] nothrow on
      plot set dip 100 ddir [i/10] distance @dist
      plot bitmap filename @fname
    endcommand
    curv = curv + inc
  endloop
end

ball attribute damp 0.0
set timestep auto
set mech age 0.0
ball result time 0.02 addattribute group addattribute velocity activate
solve time 15.0
save final
;@makeMovie(15,0.04,test)

return
;==============================================================================
; eof: Hopper.p3dat
